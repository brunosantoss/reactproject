const path = require('path');

const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
  watch: true,
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [{
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: "[name]_[local]_[hash:base64]",
              sourceMap: true,
              minimize: true
            }
          }
        ]
      }
    ]
  },
  plugins: [htmlPlugin],
  //configuracoes para inciar o servidor corretamente no CodeAnywhere container.  
  mode: 'development',
  devServer: {
    host: '0.0.0.0',
    port: 3000,        
    compress: true,
    hot: true,
    inline:true,    
    colors:true,
    contentBase: path.resolve(__dirname, "./dist"),
    allowedHosts: [
      '.codeanyapp.com'
    ],
    open: 'Google Chrome'
  }
};